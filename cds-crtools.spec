Name:           cds-crtools
Summary:        A collection of tools for gathering data from, and inducing excitations on LIGO data channels. diag, diagd, diaggui, awggui, tpcmd, chndump are included along with foton, the filter design program.
Version:        0.1.0_rc18
Source:		dtt-prerelease.tar.gz
Release:        1%{?dist}
License:        GPL2+
Requires: root-graf-x11
BuildRequires: cmake3 >= 3.13
BuildRequires: root readline-devel fftw-devel zlib-devel expat-devel
BuildRequires: cyrus-sasl-devel libcurl-devel gds-cdsbase libmetaio-devel
BuildRequires: pcre-devel freetype-devel libpng-devel openssl-devel xz-devel
BuildRequires: python2-devel
BuildRequires: python3-devel


%description
%{summary}

%prep
%setup -q -n dtt-prerelease

%build
%cmake3 -DGDS_INCLUDE_DIR:PATH=%{_includedir}/gds .
%make_build %{?smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
%make_install



%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_libdir}/lib*
%{python3_sitelib}/*
%{python_sitelib}/*

%changelog
* Sat Feb 1 2020 Michael Thomas <michael.thomas@LIGO.ORG> - 0.1.0_rc18-1
  Initial package
